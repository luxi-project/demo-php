FROM registry.cn-beijing.aliyuncs.com/rdc-builds/php-demo:7.2

COPY . /var/www/html/php-demo
RUN /etc/init.d/apache2 start
CMD ["tail", "-f", "/var/log/apache2/access.log"]